package com.example.aplicacionanimales;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = (Button) findViewById(R.id.btnardilla);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity4.class);
                startActivityForResult(intent, 0);
            }
        });
        Button btn2 = (Button) findViewById(R.id.btnburro);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), info_burro.class);
                startActivityForResult(intent, 0);
            }
        });
        Button btn3 = (Button) findViewById(R.id.btncamello);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), info_camello.class);
                startActivityForResult(intent, 0);
            }
        });
        Button btn4 = (Button) findViewById(R.id.btndelfin);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), info_delfin.class);
                startActivityForResult(intent, 0);
            }
        });
        Button btn5 = (Button) findViewById(R.id.btnjirafa);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), info_jirafa.class);
                startActivityForResult(intent, 0);
            }
        });
        Button btn6 = (Button) findViewById(R.id.btnleon);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity2.class);
                startActivityForResult(intent, 0);
            }
        });
        Button btn7 = (Button) findViewById(R.id.btnmono);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), info_mono.class);
                startActivityForResult(intent, 0);
            }
        });
        Button btn8 = (Button) findViewById(R.id.btnrana);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), info_rana.class);
                startActivityForResult(intent, 0);
            }
        });

    }
}